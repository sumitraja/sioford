#!/usr/bin/bash
export PATH=/usr/bin
compiler=dmd

function HELP {
	echo "-u <user@hostname>"
	echo "-c <compiler dmd, ldc2, ldmd2>"
	echo "-t <target e.g. build>"
	echo "-bt <build type eg unittest"
	exit 1
}

while getopts :p:u:c:t:b: optname; do
	case $optname in
	  "p")
		project=$OPTARG
		;;
	  "u")
		userHost=$OPTARG
		;;
	  "c")
		compiler=$OPTARG
		;;
	  "t")
		target=$OPTARG
		;;
	  "b")
		buildtype=--build=$OPTARG
		;;
	  "?")
		HELP
		;;
	  :)
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;
	  *)
	  # Should not occur
		echo "Unknown error while processing options"
		;;
	esac
done

shift $(( OPTIND - 1 ))

if [ -z "$project" ] 
then
	HELP
fi

if [ -z "$userHost" ] 
then
	HELP
fi
	
projectPath=`cygpath -u $project`
projectName=`basename $projectPath`
rsyncPath=${projectPath%/}

  
echo Sending $projectName using base path $rsyncPath
/usr/bin/rsync --exclude=.dub --exclude=.git -rvz $rsyncPath $userHost: && /usr/bin/ssh -C $userHost "cd $projectName && dub $target $buildtype --compiler=$compiler"

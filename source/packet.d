/*****************************************************************************
Copyright (c) 2001 - 2009, The Board of Trustees of the University of Illinois.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the
  above copyright notice, this list of conditions
  and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the University of Illinois
  nor the names of its contributors may be used to
  endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
module packet;

struct iovec {
    	void* iov_base;
		size_t iov_len;
}

enum UDTPacketType:int {
	handshake = 0,
	keepAlive = 1,
	ack = 2,
	nack = 3,
	congestion = 4,
	shutdown = 5,
	ackAck = 6,
	drop = 7,
	peerError = 8,
	userDefined = 32767
}

class UDTPacket {

protected:
   uint mHeader[4];               		// The 128-bit header field
   iovec mPacketVector[2];             // The 2-dimension vector of UDT packet [header, data]

   int __pad;
	 
public:
  @property {
		int seqNo() {           // alias: sequence number
			return mHeader[0];
		}
		
		void seqNo(int inValue) {           // alias: sequence number
			mHeader[0] = inValue;
		}
		
		// alias: message number
		int msgNo() {
			return mHeader[1];
		}
		
		void msgNo(int inValue) {      
			mHeader[0] = inValue;
		}
		
		// alias: timestamp
		int timeStamp() {                 	
			return mHeader[1];
		}
		
		void timeStamp(int inValue) {                 	
			mHeader[2] = inValue;
		}

		int id() { 
			return mHeader[3];
		}
		
		void id(int inValue) {        
			mHeader[3] = inValue;
		}

		char[] mpcData() {                 	// alias: data/control information
			return (cast(char*)mPacketVector[1].iov_base)[0 .. mPacketVector[1].iov_len];
		}
		
		void mpcData(char[] inValue) {    // alias: data/control information
			mPacketVector[1].iov_base = cast(void*)inValue;
		}
		
		auto ref packetVector() {
		  return mPacketVector;
		}
		
		auto flag() {
			return mHeader[0] >> 31;
		}
	
		// Functionality:
		//    Get the payload or the control information field length.
		// Parameters:
		//    None.
		// Returned value:
		//    the payload or the control information field length.
		
		const ulong length() {
			return mPacketVector[1].iov_len;
		}  

		 
		// Functionality:
		//    Set the payload or the control information field length.
		// Parameters:
		//    0) [in] len: the payload or the control information field length.
		// Returned value:
		//    None.

		void length(ulong len) {
			mPacketVector[1].iov_len = cast(int)len;
		}
		// Functionality:
		//    Read the packet type.
		// Parameters:
		//    None.
		// Returned value:
		//    packet type filed (000 ~ 111).

		const int type() {
			// read bit 1~15
			return (mHeader[0] >> 16) & 0x00007FFF;
		}

		// Functionality:
		//    Read the extended packet type.
		// Parameters:
		//    None.
		// Returned value:
		//    extended packet type filed (0x000 ~ 0xFFF).

		const int extendedType() {
			// read bit 16~31
			return mHeader[0] & 0x0000FFFF;
		}
		// Functionality:
		//    Read the ACK-2 seq. no.
		// Parameters:
		//    None.
		// Returned value:
		//    packet header field (bit 16~31).

		const int ackSeqNo() {
		// read additional information field
			return mHeader[1];
		}

		// Functionality:
		//    Read the message boundary flag bit.
		// Parameters:
		//    None.
		// Returned value:
		//    packet header field [1] (bit 0~1).
		const int msgBoundary(){
			// read [1] bit 0~1
			return mHeader[1] >> 30;
		}

		// Functionality:
		//    Read the message inorder delivery flag bit.
		// Parameters:
		//    None.
		// Returned value:
		//    packet header field [1] (bit 2).
		const bool msgOrderFlag() {
			// read [1] bit 2
			return (1 == ((mHeader[1] >> 29) & 1));
		}

		// Functionality:
		//    Read the message sequence number.
		// Parameters:
		//    None.
		// Returned value:
		//    packet header field [1] (bit 3~31).
		const int msgSeq() {
			// read [1] bit 3~31
			return mHeader[1] & 0x1FFFFFFF;
		}
	}
	
	static const int mPktHdrSize = 16;			// packet header size

public:

	// Functionality:
	
	//    Pack a Control packet.
	// Parameters:
	//    0) [in] pkttype: packet type field.
	//    1) [in] lparam: pointer to the first data structure, explained by the packet type.
	//    2) [in] rparam: pointer to the second data structure, explained by the packet type.
	//    3) [in] size: size of rparam, in number of bytes;
	// Returned value:
	//    None.
	
  void pack(UDTPacketType pkttype, uint lparam = -1, int[] rparam=null, int size = 0) {
    // Set (bit-0 = 1) and (bit-1~15 = type)
		mHeader[0] = 0x80000000 | (pkttype << 16);

		// Set additional information and control information field
		switch (pkttype)
		{
			case UDTPacketType.ack: //0010 - Acknowledgement (ACK)
				// ACK packet seq. no.
				if (-1 != lparam)
					 mHeader[1] = lparam;

				// data ACK seq. no. 
				// optional: RTT (microsends), RTT variance (microseconds) advertised flow window size (packets), and estimated link capacity (packets per second)
				mPacketVector[1].iov_base = cast(char *)rparam;
				mPacketVector[1].iov_len = size;

				break;

			case UDTPacketType.ackAck: //0110 - Acknowledgement of Acknowledgement (ACK-2)
				// ACK packet seq. no.
				mHeader[1] = lparam;

				// control info field should be none
				// but "writev" does not allow this
				mPacketVector[1].iov_base = cast(char *)&__pad; //NULL;
				mPacketVector[1].iov_len = 4; //0;

				break;

			case UDTPacketType.nack: //0011 - Loss Report (NAK)
				// loss list
				mPacketVector[1].iov_base = cast(char *)rparam;
				mPacketVector[1].iov_len = size;

				break;

			case UDTPacketType.congestion: //0100 - Congestion Warning
				// control info field should be none
				// but "writev" does not allow this
				mPacketVector[1].iov_base = cast(char *)&__pad; //NULL;
				mPacketVector[1].iov_len = 4; //0;

				break;

			case UDTPacketType.keepAlive: //0001 - Keep-alive
				// control info field should be none
				// but "writev" does not allow this
				mPacketVector[1].iov_base = cast(char *)&__pad; //NULL;
				mPacketVector[1].iov_len = 4; //0;

				break;

			case UDTPacketType.handshake: //0000 - Handshake
				// control info filed is handshake info
				mPacketVector[1].iov_base = cast(char *)rparam;
				mPacketVector[1].iov_len = size; //sizeof(CHandShake);

				break;

			case UDTPacketType.shutdown: //0101 - Shutdown
				// control info field should be none
				// but "writev" does not allow this
				mPacketVector[1].iov_base = cast(char *)&__pad; //NULL;
				mPacketVector[1].iov_len = 4; //0;

				break;

			case UDTPacketType.drop: //0111 - Message Drop Request
				// msg id 
				mHeader[1] = lparam;

				//first seq no, last seq no
				mPacketVector[1].iov_base = cast(char *)rparam;
				mPacketVector[1].iov_len = size;

				break;

			case UDTPacketType.peerError: //1000 - Error Signal from the Peer Side
				// Error type
				mHeader[1] = lparam;

				// control info field should be none
				// but "writev" does not allow this
				mPacketVector[1].iov_base = cast(char *)&__pad; //NULL;
				mPacketVector[1].iov_len = 4; //0;

				break;

			case UDTPacketType.userDefined: //0x7FFF - Reserved for user defined control packets
				// for extended control packet
				// "lparam" contains the extended type information for bit 16 - 31
				// "rparam" is the control information
				mHeader[0] |= lparam;

				if (null != rparam)
				{
					 mPacketVector[1].iov_base = cast(char *)rparam;
					 mPacketVector[1].iov_len = size;
				}
				else
				{
					 mPacketVector[1].iov_base = cast(char *)&__pad;
					 mPacketVector[1].iov_len = 4;
				}

				break;

			default:
				break;
		}	 
	}
}


class UDTHandShake {
public:
   void serialize(ref int[] buf) {

		int offset = 0;
		buf[offset++] = mVersion;
		buf[offset++] = mType;
		buf[offset++] = mISN;
		buf[offset++] = mMSS;
		buf[offset++] = mFlightFlagSize;
		buf[offset++] = mReqType;
		buf[offset++] = mID;
		buf[offset++] = mCookie;
		for (int i = 0; i < 4; ++ i) {
			buf[offset++] = mPeerIP[i];
		}
   }

   void deserialize(const ref int[] buf, int size) {
   		int p = 0;
		mVersion = buf[p++];
		mType = buf[p++];
		mISN = buf[p++];
		mMSS = buf[p++];
		mFlightFlagSize = buf[p++];
		mReqType = buf[p++];
		mID = buf[p++];
		mCookie = buf[p++];
		for (int i = 0; i < 4; ++ i)
		  mPeerIP[i] = buf[p++];
   }

public:
   static const int mContentSize = 48;	// Size of hand shake data

private:
   int mVersion;          // UDT version
   int mType;             // UDT socket type
   int mISN;              // random initial sequence number
   int mMSS;              // maximum segment size
   int mFlightFlagSize;   // flow control window size
   int mReqType;          // connection request type: 1: regular connection request, 0: rendezvous connection request, -1/-2: response
   int mID;				// socket ID
   int mCookie;			// cookie
   uint mPeerIP[4];		// The IP address that the peer's UDP port is bound to
};

module utils.linuxapi;
version(linux) :

import std.conv;

/* Flags for eventfd.  */
enum {
    EFD_SEMAPHORE = 00000001,
    EFD_CLOEXEC = octal!2000000,
    EFD_NONBLOCK = octal!4000
};

alias ulong eventfd_t;



/* Return file descriptor for generic event channel.  Set initial
   value to COUNT.  */
extern(C) int eventfd (int __count, int __flags);

/* Read event counter and possibly wait for events.  */
extern int eventfd_read (int __fd, eventfd_t *__value);

/* Increment event counter.  */
extern int eventfd_write (int __fd, eventfd_t __value);



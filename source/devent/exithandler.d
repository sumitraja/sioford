module devent.exithandler;
import devent.eventqueue;
import std.exception, std.conv, std.stdio;

version(Windows) {
	import core.sys.windows.windows;
}

class ExitHandler {
	
	this(EventQueue queue) {
		this.mEventQueue = queue;
		this();
		me = this;
	}
	
	void stopEventLoop() {
		mEventQueue.stopEventLoop();
	}
	
private:
	EventQueue mEventQueue;
	static ExitHandler me;
	version(Windows) {	
		this() {
			enforce(SetConsoleCtrlHandler(cast(PHANDLER_ROUTINE)&handler, TRUE ), "Failed to set control handler");
		}
	} else version(Posix) {
		import core.sys.posix.signal;
		
		this() {
			sigaction_t action;
			action.sa_handler = &handler;
            sigemptyset(&action.sa_mask);
            action.sa_flags = 0;
            enforce(sigaction(SIGTERM, &action, null) == 0, "Failed to set control handler");
            enforce(sigaction(SIGINT, &action, null) == 0, "Failed to set control handler");
		}
	}
}

version(Windows) {
	enum {
        CTRL_C_EVENT        = 0,
        CTRL_BREAK_EVENT    = 1,
        CTRL_CLOSE_EVENT    = 2,
        CTRL_LOGOFF_EVENT   = 5,
        CTRL_SHUTDOWN_EVENT = 6
	}
	private extern(Windows) BOOL handler(DWORD ctrlType) {
		switch(ctrlType) {
			case CTRL_C_EVENT: 
			case CTRL_CLOSE_EVENT: 
			case CTRL_BREAK_EVENT:
				ExitHandler.me.stopEventLoop();
				return true;
			default:
				return false;
		}
	} 	
}

version(Posix) {
	private extern(C) void handler(int ctrlType) {
		ExitHandler.me.stopEventLoop();
	} 	
}

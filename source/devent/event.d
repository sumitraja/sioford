module devent.event;
public import std.variant;
import std.uuid;
import utils.memory;

version(FreeBSD) {
    private import core.sys.freebsd.sys.event;
    enum EventFilter : uint {
        READ = EVFILT_READ,
        WRITE = EVFILT_WRITE,
        USER = EVFILT_USER
    }
} else version(linux) {
	private import core.sys.linux.epoll;
    enum EventFilter : uint {
        READ = EPOLLIN,
        WRITE = EPOLLOUT,
        USER = EPOLLIN | EPOLLONESHOT
    }
} else {
    enum EventFilter : uint {
        READ,
        WRITE,
        USER
    }
}

/** Set on an event by the event queue after processing the event */
enum EventFlag : uint {
	UNKNOWN,
	READ,
	WRITE,
	USER,
	ERROR,
	EOF
}

class Event {
public:
	
    this(Variant payload, EventFilter filter) {
        this.mPayload = payload;
        this.mEventFilter = filter;
        this.mId = cast(shared)randomUUID();
    }
    @property Variant payload() {
        return mPayload;
    }

    @property EventFilter eventFilter() {
        return mEventFilter;
    }

    @property UUID id() {
        return mId;
    }
    
    @property void flag(EventFlag flg) {
		mFlag = flg;
	}
	
	@property EventFlag flag() {
		if(mFlag == EventFlag.UNKNOWN) {
			switch(mEventFilter) {
				case EventFilter.READ : return EventFlag.READ;
				case EventFilter.WRITE : return EventFlag.WRITE;
				case EventFilter.USER : return EventFlag.USER;
				default : return EventFlag.UNKNOWN;
			}
		} else {
			return mFlag;
		}
				
	}
		
protected:
	this(UUID id, Variant payload, EventFilter filter) {
        this.mPayload = payload;
        this.mEventFilter = filter;
        this.mId = [id][0];
    }
private:
    Variant mPayload;
    EventFilter mEventFilter;
    UUID mId;
    EventFlag mFlag;
}

static UUID ShutdownEventId = parseUUID("00000000-0000-0000-0000-000000000000");

Event shutdownEvent() {
	return new Event(ShutdownEventId, Variant(), EventFilter.USER);
}

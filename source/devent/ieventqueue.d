module devent.ieventqueue;

import std.variant;
import std.stdio, std.conv;
import std.concurrency;
import devent.exithandler;

public import devent.event;

interface EventHandler {
	void handle(ref Event);
	
	void shutdown();
}

interface IEventQueue(HANDLETYPE, EVENTTYPE) {

public:
	void runEventLoop();
	
	void stopEventLoop();

	void triggerEvent(ref Event event);

	void registerHandler(EventHandler handler);

	EVENTTYPE createQueueSpecificEvent(ref Event event);
	
	@property HANDLETYPE id(); 

	version(Windows) {
		// event handling is different on windows as the IOCP is tied to a handle 
		// rather than a handle + a filter. Event will always get fired even if passing in 
		// a completion key. Will need to track events independently of the handle they
		// are associated with if the same API is to be adopted
		
        void registerEventSource(T)(T handle);
		
        void deregisterEventSource(T)(T handle);

	} else version(Posix) {
        void registerEventSource(T)(T handle, ref Event event);
		
        void deregisterEventSource(T)(T handle, EventFilter deregisterForFilter);
	}

}

abstract class EventQueueImpl(HANDLETYPE, EVENTTYPE): IEventQueue!(HANDLETYPE, EVENTTYPE) {
public:

	this(bool proxying, bool looping) {
		this.looping = looping;
		this.proxying = proxying;
	}
	
	~this() {
		if (looping) {
			stopEventLoop();
		}
	}
	
	final void registerHandler(EventHandler handler) {
		handlers ~= handler;
	}
	
	final void runEventLoop() 
	in {
		assert(looping && !proxying);
	}
	body {
	    while(looping) {
			runEventReader();
		}
	}
	
	final void stopEventLoop() {
		auto shutdown = shutdownEvent();
		triggerEvent(shutdown);
	}

	EventQueueImpl!(HANDLETYPE, EVENTTYPE) clone();

protected:
	
	final void handleEvent(ref Event event) {
		looping = event.id != ShutdownEventId;
		foreach(EventHandler handler; handlers) {
			if(!looping) {
				handler.shutdown();
			} else {
				handler.handle(event);
			}
		}
	}
	
	void runEventReader();
	
	void shutdown();
	
private:
	EventHandler[] handlers;
	bool looping;
	bool proxying;
}


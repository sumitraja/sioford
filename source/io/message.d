module io.message;

import std.socket : Address;

class IOMessage {
    this(ref char[] msgBody, Address toAddress, Address fromAddress, bool shortRead = false) {
        mMsgBody = msgBody;
        mToAddress = toAddress;
        mFromAddress = fromAddress;
        mShortRead = shortRead;
    }

    this(ref char[] msgBody, Address toAddress, bool shortRead = false) {
        mMsgBody = msgBody;
        mToAddress = toAddress;
        mShortRead = shortRead;
    }

    this(ref char[] msgBody) {
        mMsgBody = msgBody;
    }

    @property {
        ref char[] msgBody() {
            return mMsgBody;
        }

        Address toAddress() {
            return mToAddress;
        }

        Address fromAddress() {
            return mFromAddress;
        }

        ulong size() {
            return mMsgBody.length;
        }

        bool isShortRead() {
            return mShortRead;
        }

        version(Posix) {
            uint flags() {
                return mFlags;
            }

            void flags(uint set) {
                mFlags = set;
            }
        }

    }


private:
    char[] mMsgBody;
    Address mToAddress;
    Address mFromAddress;
    bool mShortRead;

    version(Posix) {
        uint mFlags;
    }

}

IOMessage[] fragmentMessage(uint fragSize, ref IOMessage source) {
    import std.stdio, std.conv;
    IOMessage ret[];
    if(source.size <= fragSize) {
        ret = ret ~ source;
    } else {
        uint size = fragSize;
        for(ulong c = 0; c < source.size; c += fragSize) {
            char[] buf;
            if (size < source.size) {
                buf = source.msgBody[cast(uint)c..size];
            } else {
                buf = source.msgBody[cast(uint)c..$];
            }
            ret = ret ~ new IOMessage(buf, source.toAddress, source.fromAddress);
            size += fragSize;
        }
    }
    return ret;
}

unittest {
    import std.stdio, std.conv, std.socket : InternetAddress;
    auto buf = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
    auto addr = new InternetAddress(InternetAddress.ADDR_ANY, 1234);
    auto addr2 = new InternetAddress(InternetAddress.ADDR_ANY, 4321);
    auto subject = new IOMessage(buf, addr, addr2);
    auto res = fragmentMessage(3, subject);

    assert(res.length == 3);
    assert(['a', 'b', 'c'] == res[0].msgBody);
    assert(['d', 'e', 'f'] == res[1].msgBody);
    assert(['g'] == res[2].msgBody);
}

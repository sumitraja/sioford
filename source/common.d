/*****************************************************************************
Copyright (c) 2001 - 2011, The Board of Trustees of the University of Illinois.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the
  above copyright notice, this list of conditions
  and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the University of Illinois
  nor the names of its contributors may be used to
  endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
module common;

import std.math;

////////////////////////////////////////////////////////////////////////////////

// UDT Sequence Number 0 - (2^31 - 1)

// seqcmp: compare two seq#, considering the wraping
// seqlen: length from the 1st to the 2nd seq#, including both
// seqoff: offset from the 2nd to the 1st seq#
// incseq: increase the seq# by 1
// decseq: decrease the seq# by 1
// incseq: increase the seq# by a given offset

class UDTSeqNo
{
public:
    static int seqcmp(int seq1, int seq2) {
        return (abs(seq1 - seq2) < mSeqNoTH) ? (seq1 - seq2) : (seq2 - seq1);
    }

    static int seqlen(int seq1, int seq2) {
        return (seq1 <= seq2) ? (seq2 - seq1 + 1) : (seq2 - seq1 + mMaxSeqNo + 2);
    }
    
    static int seqoff(int seq1, int seq2) {
       if (abs(seq1 - seq2) < mSeqNoTH)
          return seq2 - seq1;
    
       if (seq1 < seq2)
          return seq2 - seq1 - mMaxSeqNo - 1;
    
       return seq2 - seq1 + mMaxSeqNo + 1;
    }
    
    static int incseq(int seq) {
        return (seq == mMaxSeqNo) ? 0 : seq + 1;
    }
    
    static int decseq(int seq) {
        return (seq == 0) ? mMaxSeqNo : seq - 1;
    }
    
    static int incseq(int seq, int inc) {
        return (mMaxSeqNo - seq >= inc) ? seq + inc : seq - mMaxSeqNo + inc - 1;
    }
    
public:
    static const int mSeqNoTH;             // threshold for comparing seq. no.
    static const int mMaxSeqNo;            // maximum sequence number used in UDT
};
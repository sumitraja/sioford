﻿module example.echoclient;

import io.socket, io.serversocket, io.message, devent.eventqueue;
import std.conv, std.stdio, std.string, std.exception, std.algorithm;
import utils.Log;

class TCPEchoSocketStateHandler : TCPSocketStateHandler {
    
    this(TCPSocket socket, ref EventQueue eventQueue, char[] sendData) {
        super(socket, eventQueue);
        this.sendData = sendData;
    }
    
    override SocketState readyToRead() {
        auto readBuffer = buffer[currentReadIndex..$];
        auto message = socket.recvMessage(readBuffer);
        auto indexEnd = countUntil!("a == b", char[], char)(buffer, '\n');
        if(message.size > 0 && indexEnd > -1) {
            write("Server (short read = " ~ to!string(message.isShortRead) ~ ")--->" ~ to!string(buffer[0..indexEnd + 1]));
            stdout.flush();
            currentReadIndex = 0;
            return SocketState.WantWrite;
        }
        currentReadIndex += message.size;
        return SocketState.WantRead;
    }
    
    override SocketState readyToWrite() {
        auto indexEnd = countUntil!("a == b", char[], char)(sendData, '\n');
        if(indexEnd > -1) {
            auto refBuf = sendData[0..(indexEnd + 1)];
            auto msg = new IOMessage(refBuf);
            socket.sendMessage(msg);
            sendData = sendData[(indexEnd + 1)..$];
            return SocketState.WantRead;
        } else {
            return SocketState.WantClose;
        }
    }
    
    override void eof() {
        try {
            writeln("Connection with " ~ socket.remoteAddress.toString() ~ " ended");
        } catch (SocketOSException soe) {
            writeln("On EOF caught: %s", soe.msg);
        }
    }
    
    override SocketState error(int err) {
        writeln("Connection with " ~ socket.remoteAddress.toString() ~ " errored with code " ~ to!string(err));
        return SocketState.WantClose;
    }
    
    override SocketState startState() {
        return SocketState.WantWrite;
    }
    
private:
    char[] sendData;
    char[1024] buffer;
    uint currentReadIndex = 0;
}


class UDPEchoSocketStateHandler : UDPSocketStateHandler {
    
	this(UDPSocket socket, ref EventQueue eventQueue, char[] sendData, InternetAddress toAddress) {
        super(socket, eventQueue);
        this.sendData = sendData;
		this.toAddress = toAddress;
    }
    
    override SocketState readyToRead() {
        auto message = socket.recvMessage();
        if (message.size > 0) {
            auto buffer = message.msgBody;
            auto indexEnd = countUntil!("a == b", char[], char)(buffer, '\n');
            zbuff ~= buffer;
            if(indexEnd > -1  || (message.isShortRead)) {
                write("Server (short read = " ~ to!string(message.isShortRead) ~ ")--->" ~ to!string(zbuff));
                stdout.flush();
                return SocketState.WantWrite;
            } 
        }
        return SocketState.WantRead;
    }
    
    override SocketState readyToWrite() {
        auto indexEnd = countUntil!("a == b", char[], char)(sendData, '\n');
        if(indexEnd > -1) {
            auto refBuf = sendData[0..(indexEnd + 1)];
            auto msg = new IOMessage(refBuf, toAddress);
            socket.sendMessage(msg, true);
            sendData = sendData[(indexEnd + 1)..$];
            return SocketState.WantRead;
        } else {
            return SocketState.WantClose;
        }
    }
    
    override SocketState startState() {
        return SocketState.WantWrite;
    }
    
    override SocketState error(int err) {
        writeln("Connection errored with code " ~ to!string(err));
        return SocketState.WantClose;
    }
    
private:
    char[] sendData;
    char[] zbuff;
    Address toAddress;
    uint limit = 0;
}





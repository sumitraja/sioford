module example.app;
import devent.eventqueue;
import devent.ieventqueue;
import devent.exithandler;
import io.socket, io.serversocket, io.message;
import devent.queuetester, utils.container;
import std.conv, std.stdio, std.array, std.string, std.exception, std.algorithm, std.format;
import core.thread;
import std.getopt;
import example.echoclient, example.echoserver;

void main(string[] args) {
    version(unittest) {
        writeln("Ran unit tests");
    } else {
        bool client = false;
        bool udp = false;
        int connections = 1;
        string host = "localhost";
        uint ps = MTU;
        getopt(args, "client", &client, "udp", &udp, "conn", &connections, "host", &host, "packSize", &ps);
        auto queue = new EventQueue();
        auto exitHandler = new ExitHandler(queue);
        if (client) {
            import example.echoclient;
            auto socketHandlers = new Set!SocketStateHandler();
            auto eventHandler = new BasicSocketEventHandler(queue, (){socketHandlers.forEach((ref h){ h.socket.close();});});
            auto sendData =  "Hello from sioford %d\nAnd Hello again %d\n";
            auto addr = new InternetAddress(host, 1234);
            Socket socket;
            SocketStateHandler stateHandler;
            foreach(int i;0..connections) {
                auto writer = appender!string();
                formattedWrite(writer, sendData, i, i);
                if(udp) {
                    socket = new UDPSocket(AddressFamily.INET);
					stateHandler = new UDPEchoSocketStateHandler(cast(UDPSocket)socket, queue, to!(char[])(writer.data), addr);
                } else {
                    socket = new TCPSocket(AddressFamily.INET);
					stateHandler = new TCPEchoSocketStateHandler(cast(TCPSocket)socket, queue, to!(char[])(writer.data));
                }
                stateHandler.prepareForIO();
                socketHandlers.add(stateHandler);
                if (!udp) {
                    (cast(TCPSocket)socket).connect(addr);
                }
            }
        } else {
            import example.echoserver;
            auto addr = new InternetAddress(InternetAddress.ADDR_ANY, 1234);
            if(udp) {
                auto socket = new UDPSocket(AddressFamily.INET, addr);
                auto eventHandler = new BasicSocketEventHandler(queue, (){socket.close();});
                auto stateHandler = new UDPEchoSocketStateHandler(socket, queue);
                stateHandler.prepareForIO();
            } else {
                TCPSocketBoundHandler boundHandler = (TCPSocket socket) => writeln("Connection from " ~ socket.remoteAddress.toString());
                TCPSocketStateHandlerFactory factory = (TCPSocket socket, ref EventQueue queue) => new TCPEchoSocketStateHandler(socket, queue);
                auto socket = new TCPServerSocket(AddressFamily.INET, queue, factory, boundHandler);
                socket.bind(addr, 10);
            }
        }
        queue.runEventLoop();
    }
}


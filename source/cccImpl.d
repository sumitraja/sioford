/*****************************************************************************
Copyright (c) 2001 - 2011, The Board of Trustees of the University of Illinois.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the
  above copyright notice, this list of conditions
  and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the University of Illinois
  nor the names of its contributors may be used to
  endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
module cccimpl;
import core.time;
import std.conv, std.math;

import ccc, packet, common;

class CCCImpl : CCC {
private:
   long mLastRCTime;       // last rate increase time
   long mRCInterval;	   // UDT Rate control interval
   bool mSlowStart;        // if in slow start phase
   double mCWndSize;       // Congestion window size, in packets
   int mLastAck;     	   // last ACKed seq no
   double mMaxCWndSize;    // maximum cwnd size, in packets
   int mRcvRate;		   // packet arrive rate at receiver side, packets per second
   double mPktSndPeriod;   // Packet sending period, in microseconds
   int mRTT;			   // current estimated RTT, microsecond
   bool mLoss;			   // if loss happened since last rate increase
   int mBandwidth;		   // estimated bandwidth, packets per second
   double mLastDecPeriod;  // value of pktsndperiod when last decrease happened
   int mMSS;			   // Maximum Packet Size, including all packet headers
   
public:
   void init(){}
   void close(){}
   void onACK(int ack){
      
      auto currentTime = TickDuration.currSystemTick().usecs;
      
      if (currentTime - mLastRCTime < mRCInterval) {
         return;
      }
      
      mLastRCTime = currentTime;
      
      if (mSlowStart) {
         mCWndSize += UDTSeqNo.seqlen(mLastAck, ack);
         mLastAck = ack;
      
         if (mCWndSize > mMaxCWndSize) {
            mSlowStart = false;
            if (mRcvRate > 0) {
               mPktSndPeriod = 1000000.0 / mRcvRate;
            } else {
               mPktSndPeriod = (mRTT + mRCInterval) / mCWndSize;
            }
         } else {
            // During Slow Start, no rate increase
            return;
         }
      } else {
         mCWndSize = mRcvRate / 1000000.0 * (mRTT + mRCInterval) + 16;
      }
      
      if (mLoss) {
         mLoss = false;
         return;
      }
      //
      long B = to!long(mBandwidth - 1000000.0 / mPktSndPeriod);
      if ((mPktSndPeriod > mLastDecPeriod) && ((mBandwidth / 9) < B)) {
         B = mBandwidth / 9;
      }
      
      double inc = 0;
      double min_inc = 0.01;
      
      if (B <= 0) {
         inc = min_inc;
      } else {
         // inc = max(10 ^ ceil(log10( B * MSS * 8 ) * Beta / MSS, 1/MSS)
         // Beta = 1.5 * 10^(-6)
   
         inc = pow(10.0, ceil(log10(B * mMSS * 8.0))) * 0.0000015 / mMSS;
      
         if (inc < min_inc) {
            inc = min_inc;
         }
      }
      
      mPktSndPeriod = (mPktSndPeriod * mRCInterval) / (mPktSndPeriod * inc + mRCInterval);
   }
   void onLoss(const int, int){}
   void onTimeout(){}
   void onPktSent(const ref UDTPacket){}
   void onPktReceived(const ref UDTPacket){}
   void processCustomMsg(const ref UDTPacket){}
}